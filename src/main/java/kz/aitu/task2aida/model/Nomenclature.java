package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "nomenclature")
public class Nomenclature {
    @Id
    private long id;
    private String nomenclatureNumber;
    private int year;
    private long nomenclatureSummaryId;
    private long companyUnitId;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}
