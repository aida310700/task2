package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "tempfiles")
public class TempFiles {
    @Id
    private long id;
    private String fileBinary;
    private short fileBinaryByte;
}

