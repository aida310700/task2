package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "case_index")
public class CaseIndex {
    @Id
    private long id;
    private String caseIndex;
    private String titleRu;
    private String titleKz;
    private String titleEn;
    private int storageType;
    private int storageYear;
    private String note;
    private long companyUnitId;
    private long nomenclatureId;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}
