package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "file")
public class File {
    @Id
    private long id;
    private String name;
    private String type;
    private long size;
    private int pageCount;
    private String hash;
    private boolean isDeleted;
    private long fileBinaryId;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}
