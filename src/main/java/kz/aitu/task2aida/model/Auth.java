package kz.aitu.task2aida.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="auth")
public class Auth {
    @Id
    private long id;
    private String username;
    private String password;
    private String role;
    private String forgotPasswordKey;
    private String getForgotPasswordKeyTimestamp;
    private long companyUnitId;

}
