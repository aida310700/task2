package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "company_unit")
public class CompanyUnit {
    @Id
    private long id;
    private String nameRu;
    private String nameKz;
    private String nameEn;
    private long parentId;
    private int year;
    private int companyId;
    private String codeIndex;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}
