package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "case")
public class Case {
    @Id
    private long id;
    private String caseNumber;
    private String caseTom;
    private String caseHeadingRu;
    private String caseHeadingKz;
    private String caseHeadingEn;
    private long startDate;
    private long finishDate;
    private long pageNumber;
    private boolean eds;
    private String edsSignature;
    private boolean sendingNaf;
    private boolean deletionSign;
    private boolean limitedAccess;
    private String hash;
    private int version;
    private String idVersion;
    private boolean activeVersion;
    private String note;
    private long locationId;
    private long caseIndexId;
    private long inventoryId;
    private long destructionActId;
    private long structuralSubdivisionId;
    private String caseBlockchainAddress;
    private long addBlockchainDate;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}

