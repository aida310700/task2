package kz.aitu.task2aida.model;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Data
@NoArgsConstructor
@Entity
@Table(name = "company")
public class Company {
    @Id
    private long id;
    private String nameRu;
    private String nameKz;
    private String nameEn;
    private String bin;
    private long parentId;
    private long fondId;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;
}

