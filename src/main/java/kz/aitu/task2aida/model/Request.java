package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "request")
public class Request {
    @Id
    private long id;
    private long requestUserId;
    private long responseUserId;
    private long caseId;
    private long caseIndexId;
    private String createdType;
    private String comment;
    private String status;
    private long timestamp;
    private long sharestart;
    private long sharefinish;
    private boolean favorite;
    private long updateTimestamp;
    private long updateBy;
    private String declineNote;
    private long companyUnitId;
    private long fromRequestId;
}

