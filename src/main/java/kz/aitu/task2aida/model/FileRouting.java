package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "file_routing")
public class FileRouting {
    @Id
    private long id;
    private long fileId;
    private String tableName;
    private long tableId;
    private String type;
}

