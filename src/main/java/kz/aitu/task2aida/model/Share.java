package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "share")
public class Share {
    @Id
    private long id;
    private long requestId;
    private String note;
    private long senderId;
    private long receiverId;
    private long shareTimestamp;
}
