package kz.aitu.task2aida.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id
    private long id;
    private long authId;
    private String name;
    private String fullname;
    private String surname;
    private String secondname;
    private String status;
    private long companyUnitId;
    private String password;
    private long lastLoginTimestamp;
    private String iin;
    private boolean isActive;
    private boolean isActivated;
    private long createdTimestamp;
    private long createdBy;
    private long updatedTimestamp;
    private long updatedBy;}

