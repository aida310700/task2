package kz.aitu.task2aida.model;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "searchkey_routing")

public class SearchkeyRouting {
    @Id
    private long id;
    private long searchkeyId;
    private String tableName;
    private long tableId;
    private String type;

}
