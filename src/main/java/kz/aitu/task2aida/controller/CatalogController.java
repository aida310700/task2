package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.Auth;
import kz.aitu.task2aida.model.Catalog;
import kz.aitu.task2aida.service.AuthService;
import kz.aitu.task2aida.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class CatalogController {
    private CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }
    @GetMapping("/api/catalog")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(catalogService.getAll());
    }
    @GetMapping("/api/catalog/{id}")
    public ResponseEntity getCatalogById(@PathVariable int id){
        return ResponseEntity.ok(catalogService.getCatalogById(id));
    }
    @DeleteMapping("/api/deleteCatalog/{id}")
    public void deleteCatalog(@PathVariable long id){
        catalogService.deleteById(id);
    }

    @PutMapping(path = "/api/catalog")
    public ResponseEntity<?>updateCatalog(@RequestBody Catalog catalog) {
        return ResponseEntity.ok(catalogService.updateCatalog(catalog));
    }
}
