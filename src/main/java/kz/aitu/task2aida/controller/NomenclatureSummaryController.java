package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.NomenclatureSummary;
import kz.aitu.task2aida.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NomenclatureSummaryController {
    private NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }
    @GetMapping("/api/nomenclatureSummary")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }
    @GetMapping("/api/nomenclatureSummary/{id}")
    public ResponseEntity getNomenclatureSummaryById(@PathVariable int id){
        return ResponseEntity.ok(nomenclatureSummaryService.getNomenclatureSummaryById(id));
    }
    @DeleteMapping("/api/deleteNomenclatureSummary/{id}")
    public void deleteNomenclatureSummary(@PathVariable long id){
        nomenclatureSummaryService.deleteById(id);
    }

    @PutMapping(path = "/api/nomenclatureSummary")
    public ResponseEntity<?>updateNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary) {
        return ResponseEntity.ok(nomenclatureSummaryService.updateNomenclatureSummary(nomenclatureSummary));
    }

}
