package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.Company;
import kz.aitu.task2aida.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CompanyController {
    private CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
    @GetMapping("/api/company")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(companyService.getAll());
    }
    @GetMapping("/api/company/{id}")
    public ResponseEntity getCompanyById(@PathVariable int id){
        return ResponseEntity.ok(companyService.getCompanyById(id));
    }
    @DeleteMapping("/api/deleteCompany/{id}")
    public void deleteCompany(@PathVariable long id){
        companyService.deleteById(id);
    }

    @PutMapping(path = "/api/company")
    public ResponseEntity<?>updateCompany(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.updateCompany(company));
    }
}
