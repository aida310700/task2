package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.Case;
import kz.aitu.task2aida.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class CaseController {
    private CaseService caseService;

    public CaseController(CaseService caseService) {
        this.caseService = caseService;
    }
    @GetMapping("/api/case")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(caseService.getAll());
    }
    @GetMapping("/api/case/{id}")
    public ResponseEntity getCaseById(@PathVariable int id){
        return ResponseEntity.ok(caseService.getCaseById(id));
    }
    @DeleteMapping("/api/deleteCase/{id}")
    public void deleteCase(@PathVariable long id){
        caseService.deleteById(id);
    }

    @PutMapping(path = "/api/case")
    public ResponseEntity<?>updateCase(@RequestBody Case case) {
        return ResponseEntity.ok(caseService.updateCase(case));
    }

}

