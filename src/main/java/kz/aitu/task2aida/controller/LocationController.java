package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.Location;
import kz.aitu.task2aida.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class LocationController {
    private LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }
    @GetMapping("/api/location")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(locationService.getAll());
    }
    @GetMapping("/api/location/{id}")
    public ResponseEntity getLocationById(@PathVariable int id){
        return ResponseEntity.ok(locationService.getLocationById(id));
    }
    @DeleteMapping("/api/deleteLocation/{id}")
    public void deleteLocation(@PathVariable long id){
        locationService.deleteById(id);
    }

    @PutMapping(path = "/api/location")
    public ResponseEntity<?>updateLocation(@RequestBody Location location) {
        return ResponseEntity.ok(locationService.updateLocation(location));
    }

}
