package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.Searchkey;
import kz.aitu.task2aida.service.SearchkeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class SearchkeyController {
    private SearchkeyService searchkeyService;

    public SearchkeyController(SearchkeyService searchkeyService) {
        this.searchkeyService = searchkeyService ;
    }
    @GetMapping("/api/searchkey")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(searchkeyService.getAll());
    }
    @GetMapping("/api/searchkey/{id}")
    public ResponseEntity getSearchkeyById(@PathVariable int id){
        return ResponseEntity.ok(searchkeyService.getSearchkeyById(id));
    }
    @DeleteMapping("/api/deleteSearchkey/{id}")
    public void deleteSearchkey(@PathVariable long id){
        searchkeyService.deleteById(id);
    }

    @PutMapping(path = "/api/searchkey")
    public ResponseEntity<?>updateSearchkey(@RequestBody Searchkey searchkey) {
        return ResponseEntity.ok(searchkeyService.updateSearchkey(searchkey));
    }

}
