package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }
    @GetMapping("/api/record")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(recordService.getAll());
    }
    @GetMapping("/api/record/{id}")
    public ResponseEntity getRecordById(@PathVariable int id){
        return ResponseEntity.ok(recordService.getRecordById(id));
    }
    @DeleteMapping("/api/deleteRecord/{id}")
    public void deleteRecord(@PathVariable long id){
        recordService.deleteById(id);
    }

    @PutMapping(path = "/api/record")
    public ResponseEntity<?>updateRecord(@RequestBody Record record) {
        return ResponseEntity.ok(recordService.updateRecord(record));
    }

}
