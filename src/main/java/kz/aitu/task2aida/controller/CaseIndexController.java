package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.CaseIndex;
import kz.aitu.task2aida.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CaseIndexController {
    private CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }
    @GetMapping("/api/caseIndex")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(caseIndexService.getAll());
    }
    @GetMapping("/api/caseIndex/{id}")
    public ResponseEntity getCaseIndexById(@PathVariable int id){
        return ResponseEntity.ok(caseIndexService.getCaseIndexById(id));
    }
    @DeleteMapping("/api/deleteCaseIndex/{id}")
    public void deleteCaseIndex(@PathVariable long id){
        caseIndexService.deleteById(id);
    }

    @PutMapping(path = "/api/caseIndex")
    public ResponseEntity<?>updateCaseIndex(@RequestBody CaseIndex caseIndex) {
        return ResponseEntity.ok(caseIndexService.updateCaseIndex(caseIndex));
    }
}
