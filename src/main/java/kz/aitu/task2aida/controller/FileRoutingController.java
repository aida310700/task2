package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.FileRouting;
import kz.aitu.task2aida.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


    public class FileRoutingController {
        private FileRoutingService fileRoutingService;

        public FileRoutingController(FileRoutingService fileRoutingService) {
            this.fileRoutingService = fileRoutingService;
        }
        @GetMapping("/api/fileRouting")
        public ResponseEntity<?> getAll(){
            return ResponseEntity.ok(fileRoutingService.getAll());
        }
        @GetMapping("/api/fileRouting/{id}")
        public ResponseEntity getFileRoutingById(@PathVariable int id){
            return ResponseEntity.ok(fileRoutingService.getFileRoutingById(id));
        }
        @DeleteMapping("/api/deleteFileRouting/{id}")
        public void deleteFileRouting(@PathVariable long id){
            fileRoutingService.deleteById(id);
        }

        @PutMapping(path = "/api/fileRouting")
        public ResponseEntity<?>updateFileRouting(@RequestBody FileRouting fileRouting) {
            return ResponseEntity.ok(fileRoutingService.updateFileRouting(fileRouting));
        }
}
