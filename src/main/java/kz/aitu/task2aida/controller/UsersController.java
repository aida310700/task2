package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.Users;
import kz.aitu.task2aida.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class UsersController {
    private UsersService usersService;

    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }
    @GetMapping("/api/users")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(usersService.getAll());
    }
    @GetMapping("/api/users/{id}")
    public ResponseEntity getUsersById(@PathVariable int id){
        return ResponseEntity.ok(usersService.getUsersById(id));
    }
    @DeleteMapping("/api/deleteUsers/{id}")
    public void deleteAuth(@PathVariable long id){
        usersService.deleteById(id);
    }

    @PutMapping(path = "/api/users")
    public ResponseEntity<?>updateUsers(@RequestBody Users users) {
        return ResponseEntity.ok(usersService.updateUsers(users));
    }

}
