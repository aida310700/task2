package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.CatalogCase;
import kz.aitu.task2aida.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CatalogCaseController {
    private CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }
    @GetMapping("/api/catalogCase")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(catalogCaseService.getAll());
    }
    @GetMapping("/api/catalogCase/{id}")
    public ResponseEntity getCatalogCaseById(@PathVariable int id){
        return ResponseEntity.ok(catalogCaseService.getCatalogCaseById(id));
    }
    @DeleteMapping("/api/deleteCatalogCase/{id}")
    public void delete(@PathVariable long id){
        catalogCaseService.deleteById(id);
    }

    @PutMapping(path = "/api/catalogCase")
    public ResponseEntity<?>updateCatalogCase(@RequestBody CatalogCase catalogCase) {
        return ResponseEntity.ok(catalogCaseService.updateCatalogCase(catalogCase));
    }
}
