package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.Share;
import kz.aitu.task2aida.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class ShareController {
    private ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }
    @GetMapping("/api/share")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(shareService.getAll());
    }
    @GetMapping("/api/share/{id}")
    public ResponseEntity getShareById(@PathVariable int id){
        return ResponseEntity.ok(shareService.getShareById(id));
    }
    @DeleteMapping("/api/deleteShare/{id}")
    public void deleteShare(@PathVariable long id){
        shareService.deleteById(id);
    }

    @PutMapping(path = "/api/share")
    public ResponseEntity<?>updateShare(@RequestBody Share share) {
        return ResponseEntity.ok(shareService.updateShare(share));
    }

}
