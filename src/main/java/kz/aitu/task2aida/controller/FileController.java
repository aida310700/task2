package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.File;
import kz.aitu.task2aida.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class FileController {
    private FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }
    @GetMapping("/api/file")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fileService.getAll());
    }
    @GetMapping("/api/file/{id}")
    public ResponseEntity getFileById(@PathVariable int id){
        return ResponseEntity.ok(fileService.getFileById(id));
    }
    @DeleteMapping("/api/deleteFile/{id}")
    public void deleteFileService(@PathVariable long id){
        fileService.deleteById(id);
    }

    @PutMapping(path = "/api/file")
    public ResponseEntity<?>updateFile(@RequestBody File file) {
        return ResponseEntity.ok(fileService.updateFile(file));
    }
}
