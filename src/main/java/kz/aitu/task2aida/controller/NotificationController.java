package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.Notification;
import kz.aitu.task2aida.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NotificationController {
    private NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    @GetMapping("/api/notification")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(notificationService.getAll());
    }
    @GetMapping("/api/notification/{id}")
    public ResponseEntity getNotificationById(@PathVariable int id){
        return ResponseEntity.ok(notificationService.getNotificationById(id));
    }
    @DeleteMapping("/api/deleteNotification/{id}")
    public void deleteNotification(@PathVariable long id){
        notificationService.deleteById(id);
    }

    @PutMapping(path = "/api/notification")
    public ResponseEntity<?>updateNotification(@RequestBody Notification notification) {
        return ResponseEntity.ok(notificationService.updateNotification(notification));
    }

}
