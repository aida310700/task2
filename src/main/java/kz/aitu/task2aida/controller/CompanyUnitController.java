package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.CompanyUnit;
import kz.aitu.task2aida.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class CompanyUnitController {
    private CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }
    @GetMapping("/api/companyUnit")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(companyUnitService.getAll());
    }
    @GetMapping("/api/companyUnit/{id}")
    public ResponseEntity getCompanyUnitById(@PathVariable int id){
        return ResponseEntity.ok(companyUnitService.getCompanyUnitById(id));
    }
    @DeleteMapping("/api/deleteCompanyUnit/{id}")
    public void deleteCompanyUnit(@PathVariable long id){
        companyUnitService.deleteById(id);
    }

    @PutMapping(path = "/api/companyUnit")
    public ResponseEntity<?>updateCompanyUnit(@RequestBody CompanyUnit companyUnit) {
        return ResponseEntity.ok(companyUnitService.updateCompanyUnit(companyUnit));
    }
}
