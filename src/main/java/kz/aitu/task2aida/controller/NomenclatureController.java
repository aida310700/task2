package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.Nomenclature;
import kz.aitu.task2aida.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class NomenclatureController {
    private NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }
    @GetMapping("/api/nomenclature")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(nomenclatureService.getAll());
    }
    @GetMapping("/api/nomenclature/{id}")
    public ResponseEntity getNomenclatureById(@PathVariable int id){
        return ResponseEntity.ok(nomenclatureService.getNomenclatureById(id));
    }
    @DeleteMapping("/api/deleteNomenclature/{id}")
    public void deleteNomenclature(@PathVariable long id){
        nomenclatureService.deleteById(id);
    }

    @PutMapping(path = "/api/nomenclature")
    public ResponseEntity<?>updateNomenclature(@RequestBody Nomenclature nomenclature) {
        return ResponseEntity.ok(nomenclatureService.updateNomenclature(nomenclature));
    }

}
