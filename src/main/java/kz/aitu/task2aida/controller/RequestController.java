package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.Request;
import kz.aitu.task2aida.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class RequestController {
    private RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }
    @GetMapping("/api/request")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(requestService.getAll());
    }
    @GetMapping("/api/request/{id}")
    public ResponseEntity getRequestById(@PathVariable int id){
        return ResponseEntity.ok(requestService.getRequestById(id));
    }
    @DeleteMapping("/api/deleteRequest/{id}")
    public void deleteRequest(@PathVariable long id){
        requestService.deleteById(id);
    }

    @PutMapping(path = "/api/request")
    public ResponseEntity<?>updateRequest(@RequestBody Request request) {
        return ResponseEntity.ok(requestService.updateRequest(request));
    }

}
