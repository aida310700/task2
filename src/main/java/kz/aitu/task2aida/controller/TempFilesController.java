package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.TempFiles;
import kz.aitu.task2aida.service.TempFilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class TempFilesController {
    private TempFilesService tempFilesService;

    public TempFilesController(TempFilesService tempFilesService) {
        this.tempFilesService = tempFilesService;
    }
    @GetMapping("/api/tempFiles")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(tempFilesService.getAll());
    }
    @GetMapping("/api/tempFiles/{id}")
    public ResponseEntity getTempFilesById(@PathVariable int id){
        return ResponseEntity.ok(tempFilesService.getTempFilesById(id));
    }
    @DeleteMapping("/api/deleteTempFiles/{id}")
    public void deleteTempFiles(@PathVariable long id){
        tempFilesService.deleteById(id);
    }

    @PutMapping(path = "/api/tempFiles")
    public ResponseEntity<?>updateTempFiles(@RequestBody TempFiles tempFiles) {
        return ResponseEntity.ok(tempFilesService.updateTempFiles(tempFiles));
    }

}
