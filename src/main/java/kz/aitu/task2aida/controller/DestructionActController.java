package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.DestructionAct;
import kz.aitu.task2aida.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class DestructionActController {
    private DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }
    @GetMapping("/api/destructionActService")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(destructionActService.getAll());
    }
    @GetMapping("/api/destructionActService/{id}")
    public ResponseEntity getDestructionActById(@PathVariable int id){
        return ResponseEntity.ok(destructionActService.getDestructionActById(id));
    }
    @DeleteMapping("/api/deleteDestructionActService/{id}")
    public void deleteDestructionActService(@PathVariable long id){
        destructionActService.deleteById(id);
    }

    @PutMapping(path = "/api/destructionActService")
    public ResponseEntity<?>updateDestructionAct(@RequestBody DestructionAct destructionAct) {
        return ResponseEntity.ok(destructionActService.updateDestructionAct(destructionAct));
    }
}
