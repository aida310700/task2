package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.ActivityJournal;
import kz.aitu.task2aida.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {
    private final ActivityJournalService activityJournalService;

    public ActivityJournalController(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }
    @GetMapping("/api/activityJournalService")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(activityJournalService.getAll());
    }
    @GetMapping("/api/activityJournalService/{id}")
    public ResponseEntity getActivityJournalById(@PathVariable int id){
        return ResponseEntity.ok(activityJournalService.getActivityJournalRepositoryById(id));

    }
    @DeleteMapping("/api/deleteActivityJournalService/{id}")
    public void deleteActivityJournal(@PathVariable long id){
        activityJournalService.deleteById(id);
    }

    @PutMapping(path = "/api/activityJournalService")
    public ResponseEntity<?>updateActivityJournal(@RequestBody ActivityJournal activityJournal) {
        return ResponseEntity.ok(activityJournalService.updateActivityJournal(activityJournal));
    }

}