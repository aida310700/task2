package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.SearchkeyRouting;
import kz.aitu.task2aida.service.SearchkeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class SearchkeyRoutingController {
    private SearchkeyRoutingService searchkeyRoutingService;

    public SearchkeyRoutingController(SearchkeyRoutingService searchkeyRoutingService) {
        this.searchkeyRoutingService = searchkeyRoutingService;
    }
    @GetMapping("/api/searchkeyRouting")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(searchkeyRoutingService.getAll());
    }
    @GetMapping("/api/searchkeyRouting/{id}")
    public ResponseEntity getSearchkeyRoutingById(@PathVariable int id){
        return ResponseEntity.ok(searchkeyRoutingService.getSearchkeyRoutingById(id));
    }
    @DeleteMapping("/api/deleteSearchkeyRouting/{id}")
    public void deleteSearchkeyRouting(@PathVariable long id){
        searchkeyRoutingService.deleteById(id);
    }

    @PutMapping(path = "/api/searchkeyRouting")
    public ResponseEntity<?>updateSearchkeyRouting(@RequestBody SearchkeyRouting searchkeyRouting) {
        return ResponseEntity.ok(searchkeyRoutingService.updateSearchkeyRouting(searchkeyRouting));
    }

}
