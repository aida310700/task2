package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.Auth;
import kz.aitu.task2aida.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {
    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }
    @GetMapping("/api/auth")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(authService.getAll());
    }
    @GetMapping("/api/auth/{id}")
    public ResponseEntity getAuthById(@PathVariable int id){
        return ResponseEntity.ok(authService.getAuthById(id));
    }
    @DeleteMapping("/api/deleteAuth/{id}")
    public void deleteAuth(@PathVariable long id){
        authService.deleteById(id);
    }

    @PutMapping(path = "/api/auth")
    public ResponseEntity<?>updateAuth(@RequestBody Auth auth) {
        return ResponseEntity.ok(authService.updateAuth(auth));
    }

}
