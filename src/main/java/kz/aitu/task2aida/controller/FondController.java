package kz.aitu.task2aida.controller;

import kz.aitu.task2aida.model.Fond;
import kz.aitu.task2aida.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class FondController {
    private FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }
    @GetMapping("/api/fond")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fondService.getAll());
    }
    @GetMapping("/api/fond/{id}")
    public ResponseEntity getFondById(@PathVariable int id){
        return ResponseEntity.ok(fondService.getFondById(id));
    }
    @DeleteMapping("/api/deleteFond/{id}")
    public void deleteFond(@PathVariable long id){
        fondService.deleteById(id);
    }

    @PutMapping(path = "/api/fond")
    public ResponseEntity<?>updateFond(@RequestBody Fond fond) {
        return ResponseEntity.ok(fondService.updateFond(fond));
    }

}
