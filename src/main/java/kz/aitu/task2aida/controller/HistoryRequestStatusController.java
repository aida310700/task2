package kz.aitu.task2aida.controller;


import kz.aitu.task2aida.model.HistoryRequestStatus;
import kz.aitu.task2aida.service.HistoryRequestStatusService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
public class HistoryRequestStatusController {
    private HistoryRequestStatusService historyRequestStatusService;

    public HistoryRequestStatusController(HistoryRequestStatusService historyRequestStatusService) {
        this.historyRequestStatusService = historyRequestStatusService;
    }
    @GetMapping("/api/historyRequestStatus")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(historyRequestStatusService.getAll());
    }
    @GetMapping("/api/historyRequestStatus/{id}")
    public ResponseEntity getHistoryRequestStatusById(@PathVariable int id){
        return ResponseEntity.ok(historyRequestStatusService.getHistoryRequestStatusById(id));
    }
    @DeleteMapping("/api/deleteHistoryRequestStatus/{id}")
    public void deleteHistoryRequestStatus(@PathVariable long id){
        historyRequestStatusService.deleteById(id);
    }

    @PutMapping(path = "/api/historyRequestStatus")
    public ResponseEntity<?>updateHistoryRequestStatus(@RequestBody HistoryRequestStatus historyRequestStatus) {
        return ResponseEntity.ok(historyRequestStatusService.updateHistoryRequestStatus(historyRequestStatus));
    }

}
