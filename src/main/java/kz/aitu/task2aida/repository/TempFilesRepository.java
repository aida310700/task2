package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.TempFiles;
import org.springframework.data.repository.CrudRepository;

public interface TempFilesRepository extends CrudRepository<TempFiles, Long> {
    TempFiles findById(long id);
}
