package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Users;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepository extends CrudRepository<Users, Long> {
    Users findById(long id);
}
