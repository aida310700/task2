package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.ActivityJournal;
import org.springframework.data.repository.CrudRepository;

public interface ActivityJournalRepository extends CrudRepository<ActivityJournal, Long> {
    ActivityJournal findById(long id);
}

