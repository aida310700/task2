package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Auth;
import kz.aitu.task2aida.model.CaseIndex;
import org.springframework.data.repository.CrudRepository;

public interface CaseIndexRepository  extends CrudRepository<CaseIndex, Long> {
    CaseIndex findById(long id);
}

