package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Catalog;
import org.springframework.data.repository.CrudRepository;

public interface CatalogRepository extends CrudRepository<Catalog, Long> {
    Catalog findById(long id);
}
