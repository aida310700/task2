package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Auth;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends CrudRepository<Auth, Long> {
    Auth findById(long id);
}
