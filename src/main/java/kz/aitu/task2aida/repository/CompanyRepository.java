package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Long> {
    Company findById(long id);
}
