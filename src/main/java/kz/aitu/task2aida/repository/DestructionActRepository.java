package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.DestructionAct;
import org.springframework.data.repository.CrudRepository;

public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {
    DestructionAct findById(long id);
}
