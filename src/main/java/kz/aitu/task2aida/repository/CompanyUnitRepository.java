package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.CompanyUnit;
import org.springframework.data.repository.CrudRepository;

public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {
    CompanyUnit findById(long id);
}
