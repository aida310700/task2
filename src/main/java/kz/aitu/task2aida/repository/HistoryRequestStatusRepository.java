package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.HistoryRequestStatus;
import org.springframework.data.repository.CrudRepository;

public interface HistoryRequestStatusRepository  extends CrudRepository<HistoryRequestStatus, Long> {
    HistoryRequestStatus findById(long id);
}
