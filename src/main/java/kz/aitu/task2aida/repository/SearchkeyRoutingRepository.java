package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.SearchkeyRouting;
import org.springframework.data.repository.CrudRepository;

public interface SearchkeyRoutingRepository extends CrudRepository<SearchkeyRouting, Long> {
    SearchkeyRouting findById(long id);
}
