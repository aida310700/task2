package kz.aitu.task2aida.repository;


import kz.aitu.task2aida.model.CatalogCase;
import org.springframework.data.repository.CrudRepository;

public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
    CatalogCase findById(long id);
}
