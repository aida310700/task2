package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.FileRouting;
import org.springframework.data.repository.CrudRepository;

public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {
    FileRouting findById(long id);
}

