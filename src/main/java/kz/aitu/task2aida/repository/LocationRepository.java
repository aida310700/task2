package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Location;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository  extends CrudRepository<Location, Long> {
    Location findById(long id);
}
