package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Searchkey;
import org.springframework.data.repository.CrudRepository;

public interface SearchkeyRepository extends CrudRepository<Searchkey, Long> {
    Searchkey findById(long id);
}
