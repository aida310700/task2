package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Notification;
import org.springframework.data.repository.CrudRepository;

public interface NotificationRepository  extends CrudRepository<Notification, Long> {
    Notification findById(long id);

}
