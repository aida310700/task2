package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.File;
import org.springframework.data.repository.CrudRepository;

public interface FileRepository extends CrudRepository<File, Long> {
    File findById(long id);
}
