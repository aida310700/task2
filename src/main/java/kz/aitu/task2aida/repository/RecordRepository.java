package kz.aitu.task2aida.repository;

import org.springframework.data.repository.CrudRepository;

public interface RecordRepository extends CrudRepository<Record, Long> {
    Record findById(long id);
}
