package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;

public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary, Long> {
        NomenclatureSummary findById(long id);

}
