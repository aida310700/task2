package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Case;
import org.springframework.data.repository.CrudRepository;

public interface CaseRepository extends CrudRepository<Case, Long> {
    Case findById(long id);
}
