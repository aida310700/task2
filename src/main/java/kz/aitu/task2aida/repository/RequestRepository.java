package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Request;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request, Long> {
    Request findById(long id);
}
