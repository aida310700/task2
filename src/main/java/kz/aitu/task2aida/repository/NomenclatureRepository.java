package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Nomenclature;
import org.springframework.data.repository.CrudRepository;

public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
    Nomenclature findById(long id);
}
