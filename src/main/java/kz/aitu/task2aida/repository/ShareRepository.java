package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Share;
import org.springframework.data.repository.CrudRepository;

public interface ShareRepository extends CrudRepository<Share, Long> {
    Share findById(long id);
}
