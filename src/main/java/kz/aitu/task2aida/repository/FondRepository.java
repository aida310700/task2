package kz.aitu.task2aida.repository;

import kz.aitu.task2aida.model.Fond;
import org.springframework.data.repository.CrudRepository;

public interface FondRepository extends CrudRepository<Fond, Long> {
        Fond findById(long id);
}

