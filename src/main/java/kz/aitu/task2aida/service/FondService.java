package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.Fond;
import kz.aitu.task2aida.repository.FondRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class FondService {
    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    private final FondRepository fondRepository;

    public List<Fond> getAll(){
        return (List<Fond>) fondRepository.findAll();
    }

    public Fond getFondById(long id){
        return fondRepository.findById(id);
    }

    public void deleteById(long id){
        fondRepository.deleteById(id);
    }


    public Fond updateFond(@RequestBody Fond fond){
        return fondRepository.save(fond);

    }}
