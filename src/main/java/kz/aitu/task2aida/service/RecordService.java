package kz.aitu.task2aida.service;


import kz.aitu.task2aida.repository.RecordRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class RecordService {
    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    private final RecordRepository recordRepository;

    public List<Record> getAll(){
        return (List<Record>) recordRepository.findAll();
    }

    public Record getRecordById(long id){
        return recordRepository.findById(id);
    }

    public void deleteById(long id){
        recordRepository.deleteById(id);
    }


    public Record updateRecord(@RequestBody Record record){
        return recordRepository.save(record);

    }}
