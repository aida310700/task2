package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Request;
import kz.aitu.task2aida.repository.RequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class RequestService {
    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    private final RequestRepository requestRepository;

    public List<Request> getAll(){
        return (List<Request>) requestRepository.findAll();
    }

    public Request getRequestById(long id){
        return requestRepository.findById(id);
    }

    public void deleteById(long id){
        requestRepository.deleteById(id);
    }


    public Request updateRequest(@RequestBody Request request){
        return requestRepository.save(request);

    }}
