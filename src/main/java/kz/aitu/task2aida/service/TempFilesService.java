package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.TempFiles;
import kz.aitu.task2aida.repository.TempFilesRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class TempFilesService {
    public TempFilesService(TempFilesRepository tempFilesRepository) {
        this.tempFilesRepository = tempFilesRepository;
    }

    private final TempFilesRepository tempFilesRepository;

    public List<TempFiles> getAll(){
        return (List<TempFiles>) tempFilesRepository.findAll();
    }

    public TempFiles getTempFilesById(long id){
        return tempFilesRepository.findById(id);
    }

    public void deleteById(long id){
        tempFilesRepository.deleteById(id);
    }


    public TempFiles updateTempFiles(@RequestBody TempFiles tempFiles){
        return tempFilesRepository.save(tempFiles);

    }}
