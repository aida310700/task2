package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.Share;
import kz.aitu.task2aida.repository.ShareRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class ShareService {
    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    private final ShareRepository shareRepository;

    public List<Share> getAll(){
        return (List<Share>) shareRepository.findAll();
    }

    public Share getShareById(long id){
        return shareRepository.findById(id);
    }

    public void deleteById(long id){
        shareRepository.deleteById(id);
    }


    public Share updateShare(@RequestBody Share share){
        return shareRepository.save(share);

    }}
