package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.CatalogCase;
import kz.aitu.task2aida.repository.CatalogCaseRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class CatalogCaseService {
    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    private final CatalogCaseRepository catalogCaseRepository;

    public List<CatalogCase> getAll(){
        return (List<CatalogCase>) catalogCaseRepository.findAll();
    }

    public CatalogCase getCatalogCaseById(long id){
        return catalogCaseRepository.findById(id);
    }

    public void deleteById(long id){
        catalogCaseRepository.deleteById(id);
    }

    public CatalogCase updateCatalogCase(@RequestBody CatalogCase catalogCase){
        return catalogCaseRepository.save(catalogCase);

    }}
