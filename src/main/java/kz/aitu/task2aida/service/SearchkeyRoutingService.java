package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.SearchkeyRouting;
import kz.aitu.task2aida.repository.SearchkeyRoutingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class SearchkeyRoutingService {
    public SearchkeyRoutingService(SearchkeyRoutingRepository searchkeyRoutingRepository) {
        this.searchkeyRoutingRepository = searchkeyRoutingRepository;
    }

    private final SearchkeyRoutingRepository searchkeyRoutingRepository;

    public List<SearchkeyRouting> getAll(){
        return (List<SearchkeyRouting>) searchkeyRoutingRepository.findAll();
    }

    public SearchkeyRouting getSearchkeyRoutingById(long id){
        return searchkeyRoutingRepository.findById(id);
    }

    public void deleteById(long id){
        searchkeyRoutingRepository.deleteById(id);
    }


    public SearchkeyRouting updateSearchkeyRouting(@RequestBody SearchkeyRouting searchkeyRouting){
        return searchkeyRoutingRepository.save(searchkeyRouting);

    }}
