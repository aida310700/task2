package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Catalog;
import kz.aitu.task2aida.model.Company;
import kz.aitu.task2aida.repository.CompanyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class CompanyService {
    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    private final CompanyRepository companyRepository;

    public List<Company> getAll(){
        return (List<Company>) companyRepository.findAll();
    }

    public Company getCompanyById(long id){
        return companyRepository.findById(id);
    }

    public void deleteById(long id){
        companyRepository.deleteById(id);
    }

    public Company updateCompany(@RequestBody Company company){
        return companyRepository.save(company);

    }}
