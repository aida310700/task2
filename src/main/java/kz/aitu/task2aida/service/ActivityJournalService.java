package kz.aitu.task2aida.service;



import kz.aitu.task2aida.model.ActivityJournal;
import kz.aitu.task2aida.repository.ActivityJournalRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class ActivityJournalService {

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    private final ActivityJournalRepository activityJournalRepository;

    public List<ActivityJournal> getAll(){
        return (List<ActivityJournal>) activityJournalRepository.findAll();
    }

    public ActivityJournal getActivityJournalById(long id){
        return activityJournalRepository.findById(id);
    }

    public void deleteById(long id){
        activityJournalRepository.deleteById(id);
    }


    public ActivityJournal updateActivityJournal(@RequestBody ActivityJournal activityJournal) {
        return activityJournalRepository.save(activityJournal);
    }}