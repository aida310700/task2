package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.Nomenclature;
import kz.aitu.task2aida.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class NomenclatureService {
    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    private final NomenclatureRepository nomenclatureRepository;

    public List<Nomenclature> getAll(){
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public Nomenclature getNomenclatureById(long id){
        return nomenclatureRepository.findById(id);
    }

    public void deleteById(long id){
        nomenclatureRepository.deleteById(id);
    }


    public Nomenclature updateNomenclature(@RequestBody Nomenclature nomenclature){
        return nomenclatureRepository.save(nomenclature);

    }}
