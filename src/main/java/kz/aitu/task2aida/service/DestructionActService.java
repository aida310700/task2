package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.DestructionAct;
import kz.aitu.task2aida.repository.DestructionActRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class DestructionActService {
    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }

    private final DestructionActRepository destructionActRepository;

    public List<DestructionAct> getAll(){
        return (List<DestructionAct>) destructionActRepository.findAll();
    }

    public DestructionAct getDestructionActById(long id){
        return destructionActRepository.findById(id);
    }

    public void deleteById(long id){
        destructionActRepository.deleteById(id);
    }

    public DestructionAct updateDestructionAct(@RequestBody DestructionAct destructionAct){
        return destructionActRepository.save(destructionAct);

    }}

