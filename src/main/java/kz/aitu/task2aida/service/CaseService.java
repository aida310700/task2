package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.Case;
]import kz.aitu.task2aida.repository.CaseRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class CaseService {
    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    private final CaseRepository caseRepository;

    public List<Case> getAll(){
        return (List<Case>) caseRepository.findAll();
    }

    public Case getCaseById(long id){
        return caseRepository.findById(id);
    }

    public void deleteById(long id){
        caseRepository.deleteById(id);
    }

    public Case updateCase(@RequestBody Case){
        return caseRepository.save(case);

    }

}
