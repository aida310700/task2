package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.File;
import kz.aitu.task2aida.repository.FileRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service

public class FileService {
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    private final FileRepository fileRepository;

    public List<File> getAll(){
        return (List<File>) fileRepository.findAll();
    }

    public File getFileById(long id){
        return fileRepository.findById(id);
    }

    public void deleteById(long id){
        fileRepository.deleteById(id);
    }
    public File updateFile(@RequestBody File file){
        return fileRepository.save(file);

    }}
