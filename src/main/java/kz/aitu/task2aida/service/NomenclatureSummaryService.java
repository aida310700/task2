package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.NomenclatureSummary;
import kz.aitu.task2aida.repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class NomenclatureSummaryService {
    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    private final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public List<NomenclatureSummary> getAll(){
        return (List<NomenclatureSummary>) nomenclatureSummaryRepository.findAll();
    }

    public NomenclatureSummary getNomenclatureSummaryById(long id){
        return nomenclatureSummaryRepository.findById(id);
    }

    public void deleteById(long id){
        nomenclatureSummaryRepository.deleteById(id);
    }


    public NomenclatureSummary updateNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary){
        return nomenclatureSummaryRepository.save(nomenclatureSummary);

    }}
