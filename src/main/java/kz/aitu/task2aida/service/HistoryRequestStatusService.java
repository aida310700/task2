package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.HistoryRequestStatus;
import kz.aitu.task2aida.repository.HistoryRequestStatusRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class HistoryRequestStatusService {
    public HistoryRequestStatusService(HistoryRequestStatusRepository historyRequestStatusRepository) {
        this.historyRequestStatusRepository = historyRequestStatusRepository;
    }

    private final HistoryRequestStatusRepository historyRequestStatusRepository;

    public List<HistoryRequestStatus> getAll(){
        return (List<HistoryRequestStatus>) historyRequestStatusRepository.findAll();
    }

    public HistoryRequestStatus getHistoryRequestStatusById(long id){
        return historyRequestStatusRepository.findById(id);
    }

    public void deleteById(long id){
        historyRequestStatusRepository.deleteById(id);
    }


    public HistoryRequestStatus updateHistoryRequestStatus(@RequestBody HistoryRequestStatus historyRequestStatus){
        return historyRequestStatusRepository.save(historyRequestStatus);

    }}
