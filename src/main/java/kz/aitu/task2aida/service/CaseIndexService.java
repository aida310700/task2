package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.CaseIndex;
import kz.aitu.task2aida.repository.CaseIndexRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service

public class CaseIndexService {
    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    private final CaseIndexRepository caseIndexRepository;

    public List<CaseIndex> getAll(){
        return (List<CaseIndex>) caseIndexRepository.findAll();
    }

    public CaseIndex getCaseIndexById(long id){
        return caseIndexRepository.findById(id);
    }

    public void deleteById(long id){
        caseIndexRepository.deleteById(id);
    }


    public CaseIndex updateCaseIndex(@RequestBody CaseIndex caseIndex){
        return caseIndexRepository.save(caseIndex);

    }}
