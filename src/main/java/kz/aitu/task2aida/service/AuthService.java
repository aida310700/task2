package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Auth;
import kz.aitu.task2aida.repository.AuthRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class AuthService {
    public AuthService(AuthRepository authRepository) {
        this.authRepository = authRepository;
    }

    private final AuthRepository authRepository;

    public List<Auth> getAll(){
        return (List<Auth>) authRepository.findAll();
    }

    public Auth getAuthById(long id){
        return authRepository.findById(id);
    }

    public void deleteById(long id){
        authRepository.deleteById(id);
    }


    public Auth updateAuth(@RequestBody Auth auth){
        return authRepository.save(auth);

    }

}
