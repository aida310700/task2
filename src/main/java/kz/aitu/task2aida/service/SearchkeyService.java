package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Searchkey;
import kz.aitu.task2aida.repository.SearchkeyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class SearchkeyService {
    public SearchkeyService(SearchkeyRepository searchkeyRepository) {
        this.searchkeyRepository = searchkeyRepository;
    }

    private final SearchkeyRepository searchkeyRepository;

    public List<Searchkey> getAll(){
        return (List<Searchkey>) searchkeyRepository.findAll();
    }

    public Searchkey getSearchkeyById(long id){
        return searchkeyRepository.findById(id);
    }

    public void deleteById(long id){
        searchkeyRepository.deleteById(id);
    }


    public Searchkey updateSearchkey(@RequestBody Searchkey searchkey){
        return searchkeyRepository.save(searchkey);

    }}
