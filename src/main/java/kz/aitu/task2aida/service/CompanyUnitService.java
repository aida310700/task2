package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Company;
import kz.aitu.task2aida.model.CompanyUnit;
import kz.aitu.task2aida.repository.CompanyRepository;
import kz.aitu.task2aida.repository.CompanyUnitRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
@Service
public class CompanyUnitService {
    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    private final CompanyUnitRepository companyUnitRepository;

    public List<CompanyUnit> getAll(){
        return (List<CompanyUnit>) companyUnitRepository.findAll();
    }

    public CompanyUnit getCompanyUnitById(long id){
        return companyUnitRepository.findById(id);
    }

    public void deleteById(long id){
        companyUnitRepository.deleteById(id);
    }

    public CompanyUnit updateCompanyUnit(@RequestBody CompanyUnit companyUnit){
        return companyUnitRepository.save(companyUnit);

    }}
