package kz.aitu.task2aida.service;

;
import kz.aitu.task2aida.model.Catalog;
import kz.aitu.task2aida.repository.CatalogRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@Service
public class CatalogService {
    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    private final CatalogRepository catalogRepository;

    public List<Catalog> getAll(){
        return (List<Catalog>) catalogRepository.findAll();
    }

    public Catalog getCatalogById(long id){
        return catalogRepository.findById(id);
    }

    public void deleteById(long id){
        catalogRepository.deleteById(id);
    }

    public Catalog updateCatalog(@RequestBody Catalog catalog){
        return catalogRepository.save(catalog);

    }}

