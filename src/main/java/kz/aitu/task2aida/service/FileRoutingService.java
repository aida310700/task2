package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.FileRouting;
import kz.aitu.task2aida.repository.FileRoutingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class FileRoutingService {
    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    private final FileRoutingRepository fileRoutingRepository;

    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRepository.findAll();
    }

    public FileRouting getFileRoutingById(long id){
        return fileRoutingRepository.findById(id);
    }

    public void deleteById(long id){
        fileRoutingRepository.deleteById(id);
    }


    public FileRouting updateFileRouting(@RequestBody FileRouting fileRouting){
        return fileRoutingRepository.save(fileRouting);

    }}
