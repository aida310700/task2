package kz.aitu.task2aida.service;


import kz.aitu.task2aida.model.Location;
import kz.aitu.task2aida.repository.LocationRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class LocationService {
    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    private final LocationRepository locationRepository;

    public List<Location> getAll(){
        return (List<Location>) locationRepository.findAll();
    }

    public Location getLocationById(long id){
        return locationRepository.findById(id);
    }

    public void deleteById(long id){
        locationRepository.deleteById(id);
    }

    public Location updateLocation(@RequestBody Location location){
        return locationRepository.save(location);

    }}
