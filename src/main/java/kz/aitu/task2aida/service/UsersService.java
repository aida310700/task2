package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Auth;
import kz.aitu.task2aida.model.Users;
import kz.aitu.task2aida.repository.AuthRepository;
import kz.aitu.task2aida.repository.UsersRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class UsersService {
    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    private final UsersRepository usersRepository;

    public List<Users> getAll(){
        return (List<Users>) usersRepository.findAll();
    }

    public Users getUsersById(long id){
        return usersRepository.findById(id);
    }

    public void deleteById(long id){
        usersRepository.deleteById(id);
    }


    public Users updateUsers(@RequestBody Users users){
        return usersRepository.save(users);

    }}
