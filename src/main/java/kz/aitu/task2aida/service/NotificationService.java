package kz.aitu.task2aida.service;

import kz.aitu.task2aida.model.Notification;
import kz.aitu.task2aida.repository.NotificationRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class NotificationService {
    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    private final NotificationRepository notificationRepository;

    public List<Notification> getAll(){
        return (List<Notification>) notificationRepository.findAll();
    }

    public Notification getNotificationById(long id){
        return notificationRepository.findById(id);
    }

    public void deleteById(long id){
        notificationRepository.deleteById(id);
    }

    public Notification updateNotification(@RequestBody Notification notification) {
        return notificationRepository.save(notification);
    }
    }
