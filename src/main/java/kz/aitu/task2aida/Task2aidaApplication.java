package kz.aitu.task2aida;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task2aidaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task2aidaApplication.class, args);
	}

}
