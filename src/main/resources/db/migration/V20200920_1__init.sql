drop table if exists auth cascade;
create table auth (
	id serial not null,
	username varchar(255),
	email varchar (255),
	password varchar (128),
	role varchar (255),
	forgot_password_key varchar(128),
	forgot_password_key_timestamp bigint,
	company_unit_id bigint,
	primary key (id)
);

drop table if exists users cascade;
create table users (
    id serial not null,
    auth_id bigint,
    name varchar(128),
    fullname varchar (128),
    surname varchar (128),
    secondname varchar (128),
    status varchar (128),
    company_unit_id bigint,
    password varchar (128),
    last_login_timestamp bigint,
    iin varchar (32),
    is_active boolean,
    is_activated boolean,
    created_timestamp bigint,
    created_by bigint,
    updated_timestamp bigint,
    updated_by bigint,
    primary key (id)
);

drop table if exists company_unit cascade;
create table company_unit (
	id serial not null,
	name_ru varchar(128),
    name_kz varchar (128),
	name_en varchar (128),
	parent_id bigint,
	year INT,
    company_id INT,
	code_index varchar (16),
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists company cascade;
create table company (
	id serial not null,
	name_ru varchar(128),
    name_kz varchar (128),
	name_en varchar (128),
	bin varchar(32),
    parent_id bigint,
	fond_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists fond cascade;
create table fond (
	id serial not null,
    fond_number varchar(128),
    created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists record cascade;
create table record (
	id serial not null,
    number varchar (128),
    type varchar (128),
    company_unit_id bigint,
    created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists case_index cascade;
create table case_index (
	id serial not null,
	case_index varchar (128),
    title_ru varchar (128),
	title_kz varchar (128),
	title_en varchar (128),
	storage_type int,
	storage_year int,
	note varchar(128),
	company_unit_id bigint,
	nomenclature_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)

);
drop table if exists nomenclature cascade;
create table nomenclature (
	id serial not null,
    nomenclature_number varchar(128),
    year int,
    nomenclature_summary_id bigint,
    company_unit_id bigint,
    created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists nomenclature_summary cascade;
create table nomenclature_summary (
	id serial not null,
    number varchar(128),
    year int,
    company_unit_id bigint,
    created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);


drop table if exists case cascade;
create table case (
	id serial not null,
	case_number varchar (128),
	case_tom varchar (128),
	case_heading_ru varchar (128),
	case_heading_kz varchar (128),
	case_heading_en varchar (128),
	start_date bigint,
	finish_date bigint,
	page_number bigint,
	eds boolean,
	eds_signature text,
	sending_naf boolean,
	deletion_sign boolean,
	limited_access boolean,
	hash varchar (128),
	version int,
	id_ersion varchar (128),
	active_version boolean,
	note varchar(255),
	location_id bigint,
	case_index_id bigint,
	inventory_id bigint,
	destruction_act_id bigint,
	structural_subdivision_id bigint,
	case_blockchain_address varchar(128),
	add_blockchain_date bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists request cascade ;
create table request (
	id serial not null,
    request_user_id bigint,
    response_user_id bigint,
    case_id bigint,
    case_index_id bigint,
    created_type varchar (64),
    comment varchar (255),
    status varchar (64),
	timestamp bigint,
	sharestart bigint,
	sharefinish bigint,
	favorite boolean,
	update_timestamp bigint,
	update_by bigint,
	decline_note varchar (255),
	company_unit_id bigint,
	from_request_id bigint,
	primary key (id)
);

drop table if exists share cascade;
create table share (
	id serial not null,
    request_id bigint,
    note varchar(255),
    sender_id bigint,
    receiver_id bigint,
    share_timestamp bigint,
	primary key (id)
);

drop table if exists history_request_status CASCADE;
create table history_request_status (
	id serial not null,
	request_id bigint,
    status varchar(64),
    created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists catalog cascade;
create table catalog(
	id serial not null,
	name_ru varchar(128),
    name_kz varchar (128),
	name_en varchar (128),
	parent_id bigint,
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);


drop table if exists catalog_case cascade;
create table catalog_case (
	id serial not null,
    case_id bigint,
	catalog_id bigint,
	company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);


drop table if exists location cascade;
create table location (
	id serial not null,
    row varchar (64),
    line varchar (64),
    columnn varchar (64),
    box varchar (64),
    company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)

);

drop table if exists activity_journal cascade;
create table activity_journal (
	id serial not null,
	event_type varchar (128),
	object_type varchar (255),
	object_id bigint,
	created_timestamp bigint,
	created_by bigint,
	message_level varchar (128),
	message varchar (255),
	primary key (id)
);

drop table if exists notification cascade;
create table notification (
	id serial not null,
    object_type varchar(128),
    object_id bigint,
    company_unit_id bigint,
    user_id bigint,
    created_timestamp bigint,
	viewed_timestamp bigint,
	is_viewed boolean,
	title varchar (255),
	text varchar (255),
	company_id bigint,
	primary key (id)
);

drop table if exists file_routing cascade;
create table file_routing (
	id serial not null,
	file_id bigint,
    table_name varchar (128),
    table_id bigint,
    type varchar (128),
	primary key (id)
);

drop table if exists file cascade;
create table file (
	id serial not null,
	name varchar(128),
    type varchar(128),
    size bigint,
    page_count int,
    hash varchar(128),
    is_deleted boolean,
	file_binary_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);

drop table if exists temp_files cascade;
create table temp_files (
	id serial not null,
    file_binary text,
    file_binary_byte smaillint,
	primary key (id)
);

drop table if exists searchkey cascade;
create table searchkey (
	id serial not null,
    name varchar(128),
    company_unit_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)

);

drop table if exists searchkey_routing cascade;
create table searchkey_routing (
	id serial not null,
    search_key_id bigint,
    table_name varchar(128),
    table_id bigint,
	type varchar (128),
	primary key (id)
);

drop table if exists destruction_act cascade;
create table destruction_act (
	id serial not null,
	act_number varchar(128),
    base varchar(128),
	structural_subdivision_id bigint,
	created_timestamp bigint,
	created_by bigint,
	updated_timestamp bigint,
	updated_by bigint,
	primary key (id)
);



